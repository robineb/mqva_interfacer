from setuptools import setup, find_namespace_packages

with open("requirements.txt") as f:
    install_reqs = f.read().strip().split("\n")

# Filter out comments/hashes
requirements = [
    req.rstrip(" \\")
    for req in install_reqs
    if req.rstrip(" \\")
    and not req.startswith("#")  # comments
    and not req.strip().startswith("--hash=")  # hash of the package
]

setup(
    name="mqva.interfacer",
    version="0.1.0",
    license="MIT license",
    url="https://gitlab.mpcdf.mpg.de/mqv-atoms/mqva_interfacer",
    description="mqv-atoms interfacer for qiskit programs",
    long_description="mqv-atoms interfacer for qiskit programs",
    author="Stepan Snigirev, Robin Eberhard",
    author_email="stepan@planqc.eu, robin.eberhard@mpq.mpg.de",
    packages=find_namespace_packages("src", include=["mqva.*"]),
    package_dir={"": "src"},
    package_data={},
    include_package_data=True,
    install_requires=requirements,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
