import logging

from qiskit.providers import BackendV1
from qiskit.providers import Options
from qiskit.providers.models import BackendConfiguration
from qiskit.providers.models import BackendStatus

from .job import Job

logger = logging.getLogger(__name__)


class MQVBackend(BackendV1):
    def __init__(self, provider, name):
        self._name = name
        self._provider = provider

        super().__init__(
            configuration=BackendConfiguration.from_dict(self._get_config()),
            provider=provider,
        )

    def _get_config(self):
        res = self.request("/config/")
        return res["config"]

    @classmethod
    def _default_options(cls):
        return Options(shots=1024)

    def run(self, circ, **kwargs):
        job = Job(self, None, circ, **kwargs)
        job.submit()
        return job

    def request(self, endpoint, method="GET", **kwargs):
        return self._provider.request(
            f"/backends/{self._name}{endpoint}", method, **kwargs
        )

    def retrieve_job(self, job_id):
        raise NotImplementedError()

    def retrieve_jobs(self):
        return self.request("/jobs/")

    def status(self):
        """Return a backend status object to the caller.

        Returns:
            BackendStatus: the status of the backend.
        """
        # TODO
        return BackendStatus(
            backend_name=self.name(),
            backend_version="1",
            operational=True,
            pending_jobs=0,
            status_msg="",
        )
