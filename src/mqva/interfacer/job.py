from qiskit.providers import JobV1, jobstatus
from qiskit.result import Result


class Job(JobV1):
    _status = None
    _result = None

    def __init__(self, backend, job_id, circ=None, **kwargs):
        super().__init__(backend, job_id)
        self._result = None
        self._shots = kwargs.get("shots", 100)
        # copy-paste from ionq
        if circ:
            self.circuit = circ
            self._status = jobstatus.JobStatus.INITIALIZING
        else:  # retrieve existing job
            self.circuit = None
            self._status = jobstatus.JobStatus.INITIALIZING
            self.status()

    def request(self, endpoint, method="GET", **kwargs):
        return self._backend.request(
            f"/jobs/{self._job_id}{endpoint}", method, **kwargs
        )

    def cancel(self):
        raise NotImplementedError()

    def submit(self):
        if not self.circuit:
            raise RuntimeError("Can't submit without circuit")

        data = {
            "shots": self._shots,
            "qasm": self.circuit.qasm(),
        }
        res = self._backend.request("/jobs/", method="POST", json=data)
        self._job_id = res["id"]

    def _get_result(self):
        res = self.request("/result/")
        c = self._backend.configuration()
        # inject name of our circuit
        if self.circuit:
            if "name" in res["result"]:
                res["result"]["name"] = self.circuit.name
            if "name" in res["result"].get("header", {}):
                res["result"]["header"]["name"] = self.circuit.name
        if not res["result"]["success"]:
            err = res["result"]["error"]
            raise RuntimeError(f"Server failed to run a job: {err}")
        self._result = Result.from_dict(
            {
                "backend_name": c.backend_name,
                "backend_version": c.backend_version,
                "job_id": self._job_id,
                "qobj_id": self._job_id,  # not sure what should go here
                "success": res["result"]["success"],
                "results": [res["result"]],
                "status": "COMPLETED",
                "header": {
                    "backend_name": c.backend_name,
                    "backend_version": c.backend_version,
                },
                "time_taken": res["result"]["time_taken"],
            }
        )

    def status(self):
        res = self.request("/")
        status = res.get("status")
        if status:
            self._status = jobstatus.JobStatus[status]
        if self._status != jobstatus.JobStatus.VALIDATING:
            self._get_result()
        return self._status

    def result(self, wait=3):
        if self._result is not None:
            return self._result
        self.wait_for_final_state(wait=wait)
        return self._result

    def get_counts(self, experiment=None):
        return self.result().get_counts(experiment)
