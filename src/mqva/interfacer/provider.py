import logging
import os
import hmac
import requests

from qiskit.providers.providerutils import filter_backends
from qiskit.providers.exceptions import QiskitBackendNotFoundError

from .backend import MQVBackend

logger = logging.getLogger(__name__)


class MQVAProvider:
    # TODO: change local development server to real one
    DEFAULT_URL = "https://compute.mqv-atoms.de/api/"
    PASSWORD_SALT = b"mqv-atoms/password_salt"
    name = "mqv_provider"

    def __init__(self, url=None):
        # get url from argument, env variable or set to default
        url = url or os.environ.get("MQVA_QISKIT_API") or self.DEFAULT_URL
        self._url = url.rstrip("/")  # remove trailing slash
        self._session = requests.session()

        # See authorize to get a token
        self._token = None

    def request(self, endpoint, method="GET", **kwargs):
        # convert dict or list to json string
        res = self._session.request(
            method, f"{self.url}{endpoint}", headers=self.headers, **kwargs
        )
        data = res.json()
        if res.status_code < 200 or res.status_code >= 300:
            raise RuntimeError(
                data.get("message", f"API error, code {res.status_code}")
            )
        return data

    def authorize(self, username=None, password=None, token=None):
        """Authorize with username & password or with token"""
        if token:
            # check token is valid
            self._token = token
            self.request("/user/token/")
            return
        # hash password to avoid sending it as plain text
        # salt is used to avoid table lookups for weak passwords
        password = hmac.new(
            self.PASSWORD_SALT, password.encode("utf-8"), digestmod="sha256"
        ).hexdigest()

        data = {
            "username": username,
            "password_hmac": password,
        }
        r = self.request("/user/authorize/", method="POST", json=data)
        self._token = r["token"]

    @property
    def url(self):
        return self._url

    @property
    def token(self):
        return self._token

    @property
    def headers(self):
        return {
            "Content-type": "application/json",
            "Authorization": f"token {self.token}",
        }

    def backends(self):
        return self.request("/backends/")

    def get_backend(self, name=None, **kwargs):
        backends = [b["name"] for b in self.backends()]
        if not name in backends:
            raise ValueError(
                f"Invalid backend {name}. Available backends are: {backends}"
            )
        return MQVBackend(self, name)

    def __str__(self):
        return f"MQVAProvider({self.name}"

    def __repr__(self):
        return f"<{str(self)}>"
