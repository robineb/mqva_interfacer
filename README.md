# MQV-Atoms Qiskit Provider

MQV-Atoms cloud integration to qiskit framework.

## Usage

Install for development (without `-e` for production):

```sh
# create and activate virtual environment
virtualenv .venv
source .venv/bin/activate
# install dependencies
pip3 install -e .
```

Example how to use MQV-Atoms backend to calculate Qiskit circuit:

```py
import qiskit
from mqva.qiskit import MQVProvider

# create your cirquit
circ = qiskit.QuantumCircuit(3)
circ.h(0)
circ.cx(0, 1)
circ.cx(1, 2)
circ.measure_all()

# login to MQVA Cloud using credentials
mqv = MQVProvider()
mqv.authorize("username", "password")
# or use a token
# mqv.authorize(token="fa37da6e01e1743f1464c163ec7917201a1533f44a51ccdb2bb068f6ff975b98")

# list available backend
print("Available backends:")
print("\n".join([f"- {backend}" for backend in mqv.backends()]))

# select specific backend you want to use:
sim = mqv.get_backend("mqv-simulator")

# run job on the MQVA cloud
job = sim.run(circ, shots=123)
print("Waiting for the job to be complete...")
res = job.result()
print("Job done!")
print("Results:", res.get_counts())
```

## Development and testing

Install dev requirements:

```sh
pip3 install -r dev_requirements.txt
```

Set up pre-commit hooks that will enforce black formatting etc (optional):

```sh
pre-commit install
```

Run tests from the root folder:

```sh
pytest
```
